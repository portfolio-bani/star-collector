using UnityEngine;

public class QuitMenu : MonoBehaviour
{
    void Update()
    {
        if (Input.GetMouseButtonDown(0)) // Check for left mouse button (Mouse Button 0) press
        {
            Quit();
        }
    }

    public void Quit()
    {
        Application.Quit();
        Debug.Log("Quit button");
    }
}
