using UnityEngine;
using UnityEngine.SceneManagement;

public class TouchButton : MonoBehaviour
{
    private int tapCount = 0;
    private float doubleTapTimeThreshold = 0.3f; // Set the time threshold for a double tap.

    void Update()
    {
        if (Input.GetMouseButtonDown(0)) // Check for touch (left mouse button for testing).
        {
            tapCount++;

            if (tapCount == 1)
            {
                // First tap - start the timer for a potential double tap.
                Invoke("SingleTap", doubleTapTimeThreshold);
            }
            else if (tapCount == 2)
            {
                // Second tap - perform "Quit Game" action for a double tap.
                CancelInvoke("SingleTap");
                QuitGame();
                tapCount = 0;
            }
        }
    }

    void SingleTap()
    {
        // Perform "Play Again" (load previous scene) action for a single tap.
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int previousSceneIndex = Mathf.Max(0, currentSceneIndex - 1);
        SceneManager.LoadScene(previousSceneIndex);
        tapCount = 0;
    }

    void QuitGame()
    {
        Debug.Log("QuitButton");
        // Implement your "Quit Game" logic here, e.g., exit the application.
        Application.Quit();
       
    }
}
