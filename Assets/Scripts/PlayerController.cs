using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float sensitivity = 5f;
    public Camera cam;


    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }
    private void Update()
    {
        PlayerRotation();
    }

    private void PlayerRotation()
    {
        float rotateY = Input.GetAxisRaw("Mouse X");
        Vector3 rotation = new Vector3(0, rotateY, 0) * sensitivity;
        transform.Rotate(rotation);
        float rotateX = Input.GetAxisRaw("Mouse Y");
        Vector3 camRotation = new Vector3(rotateX, 0, 0) * sensitivity; 
        cam.transform.Rotate(-camRotation);
    }

}
