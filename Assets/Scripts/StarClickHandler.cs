using UnityEngine;

public class StarClickHandler : MonoBehaviour
{
    public int pointsForStar = 1; // Define how many points a star is worth when destroyed.
   
   
   

    public void OnPointerEnter()
    {
        MeshRenderer meshRenderer = GetComponent < MeshRenderer>();
        
        
        meshRenderer.material.SetColor("_EmissionColor", Color.red); // Apply the modified material.
    }

    public void OnPointerExit()
    {
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
       

        meshRenderer.material.SetColor("_EmissionColor", Color.yellow); // Restore the original material.
    }

    public void OnPointerClick()
    {
        ScoreManager scoreManager = FindObjectOfType<ScoreManager>();
        if (scoreManager != null)
        {
            scoreManager.AddScore(pointsForStar);
        }
        Destroy(gameObject);
    }
}
