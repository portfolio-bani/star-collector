using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public float totalTime = 10.0f; // Set the total time to 10 seconds.
    public TextMeshProUGUI countdownText; // Reference to the TextMesh Pro Text element for displaying the countdown.
    public int nextSceneIndex = 1; // Set the next scene index in the Inspector.

    private float currentTime;

    void Start()
    {
        ResetTimer();
    }

    void Update()
    {
        if (currentTime > 0)
        {
            currentTime -= Time.deltaTime;
            UpdateCountdownDisplay();
        }
        else
        {
            LoadNextScene();
        }
    }

    void UpdateCountdownDisplay()
    {
        int seconds = Mathf.CeilToInt(currentTime);
        countdownText.text = "Time: " + seconds.ToString();
    }

    void ResetTimer()
    {
        currentTime = totalTime;
    }

    void LoadNextScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int sceneCount = SceneManager.sceneCountInBuildSettings;
        int nextSceneIndex = (currentSceneIndex + 1) % sceneCount;
        SceneManager.LoadScene(nextSceneIndex);
    }
}
