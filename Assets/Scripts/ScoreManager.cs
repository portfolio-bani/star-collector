using UnityEngine;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    public TextMeshProUGUI scoreText; // Reference to the TextMesh Pro Text element for displaying the score.
    private int score = 0; // Initialize the score to 0.

    void Start()
    {
        UpdateScoreDisplay();
    }

    // Add a method to update the score when a star is destroyed.
    public void AddScore(int points)
    {
        score += points;
        UpdateScoreDisplay();
    }

    void UpdateScoreDisplay()
    {
        scoreText.text = "Score: " + score.ToString();
    }
}
