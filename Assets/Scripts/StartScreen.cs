using UnityEngine;
using UnityEngine.SceneManagement;

public class StartScreen : MonoBehaviour
{
    // Add a reference to the Canvas that you want to hide when the game starts.
    public Canvas startCanvas;

    private bool gameStarted = false;

    void Update()
    {
        if (!gameStarted && Input.GetButtonDown("Submit")) // Adjust "Submit" to your desired input key.
        {
            // Hide the start canvas when the game starts.
            startCanvas.gameObject.SetActive(false);

            // Set a flag to indicate that the game has started.
            gameStarted = true;

            // Resume the game or execute any other actions.
            Time.timeScale = 1.0f;
        }
    }
}
