using UnityEngine;

public class StarGenerator : MonoBehaviour
{
    public GameObject starPrefab;
    public float minDistance = 10.0f; // Minimum distance from the player.
    public float maxDistance = 20.0f; // Maximum distance from the player.
    public float minStarSize = 0.4f;  // Minimum star size.
    public float maxStarSize = 0.6f;  // Maximum star size.

    private float timeSinceLastStar = 0f;
    private Transform playerCamera;

    void Start()
    {
        playerCamera = Camera.main.transform; // Find the main camera in the scene.
        GenerateStar(); // Generate the first star immediately when the scene starts.
    }

    void Update()
    {
        timeSinceLastStar += Time.deltaTime;

        if (timeSinceLastStar >= 3.0f)
        {
            GenerateStar();
            timeSinceLastStar = 0f; // Reset the time counter.
        }

    }

    void GenerateStar()
    {
        Vector3 randomOffset = Random.onUnitSphere * Random.Range(minDistance, maxDistance);
        Vector3 starPosition = playerCamera.position + randomOffset;
        GameObject newStar = Instantiate(starPrefab, starPosition, Quaternion.identity);

        // Randomize the star's size.
        float randomSize = Random.Range(minStarSize, maxStarSize);
        newStar.transform.localScale = new Vector3(randomSize, randomSize, randomSize);

        // Rotating the star to look at the camera.
        newStar.transform.LookAt(new Vector3(0, 0, 0));
    }
}
